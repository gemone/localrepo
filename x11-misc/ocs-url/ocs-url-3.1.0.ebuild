# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils xdg

QTILVER="0.4.0"
QTIL="qtil-${QTILVER}"

DESCRIPTION="An install helper program for items served via OpenCollaborationServices (ocs://)."
HOMEPAGE="https://www.opencode.net/dfn2/ocs-url"
SRC_URI="
	https://github.com/akiraohgaki/qtil/archive/v${QTILVER}.tar.gz -> qtil.tar.gz
	https://www.opencode.net/dfn2/ocs-url/-/archive/release-${PV}/ocs-url-release-${PV}.tar.gz -> ocs-url.tar.gz
"

LICENSE="GPL-3"
RESTRICT="strip"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""


DEPEND="
	>=dev-qt/qtsvg-5:5=
	>=dev-qt/qtdeclarative-5:5=
	>=dev-qt/qtquickcontrols-5:5=
"

RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${PN}-release-${PV}"

src_compile() {
	mv ${WORKDIR}/${QTIL} ${S}/lib/qtil
	# 编译
	cd ${S}
	eqmake5 PREFIX="/usr"
	emake
}

src_install() {
	exeinto /usr/bin
	exeopts -m0755
	doexe ${S}/${PN}

	insinto /usr/share/applications
	doins ${S}/desktop/ocs-url.desktop

	insinto /usr/share/icons/hicolor/scalable/apps
	doins ${S}/desktop/ocs-url.svg
}

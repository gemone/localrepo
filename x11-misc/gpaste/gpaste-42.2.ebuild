# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson vala gnome2-utils

DESCRIPTION="Clipboard management system"
HOMEPAGE="https://hosted.weblate.org/projects/gpaste/gpaste/"
SRC_URI="https://github.com/Keruspe/GPaste/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="strip"

IUSE="X +dbus wayland gnome introspection systemd vala bash-completion zsh-completion"

# wayland 环境下在 gnome 生效，其他情况不需要
REQUIRED_USE="
dbus
wayland? ( gnome )
gnome? ( introspection )
vala? ( introspection )
"


DEPEND="
dev-libs/appstream-glib
x11-libs/pango
>=app-crypt/gcr-3.41.0
>=x11-libs/gdk-pixbuf-2.38.0
>=dev-libs/glib-2.70.0
>=gui-libs/libadwaita-1.1
>=gui-libs/gtk-4.6.0
>=x11-libs/gtk+-3.24.0

X? (
x11-libs/libX11
x11-libs/libXi
>=x11-libs/gtk+-3.24.0[X]
)

dbus? (
>=sys-apps/dbus-1.15.2
)

introspection? (
>=x11-wm/mutter-${PV}
>=dev-libs/gjs-1.72.2
>=dev-libs/gobject-introspection-1.72.2
)

systemd? (
sys-apps/systemd-utils[sysusers]
)
"
RDEPEND="${DEPEND}"
BDEPEND="
virtual/pkgconfig
gnome? (
>=gnome-base/gnome-control-center-${PV}
>=gnome-base/gnome-shell-${PV}
)
systemd? (
sys-apps/systemd
)
vala? ( $(vala_depend) )
"

S="${WORKDIR}/GPaste-${PV}"

src_prepare() {
	use vala && vala_src_prepare
	default
}

src_configure() {
	local emesonargs=(
	$(meson_use systemd systemd)
	$(meson_use introspection introspection)
	$(meson_use vala vapi)
	$(meson_use X x-keybinder)
	$(meson_use bash-completion bash-completion)
	$(meson_use zsh-completion zsh-completion)
	-Dcontrol-center-keybindings-dir=$(usex gnome \
		'/usr/share/gnome-control-center/keybindings/' \
		'')
	)

	meson_src_configure
}

pkg_postinst() {
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_schemas_update
}

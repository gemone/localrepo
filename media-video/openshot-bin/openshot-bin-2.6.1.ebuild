# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

DESCRIPTION="OpenShot Video Editor is an award-winning free and open-source video editor for Linux, Mac, and Windows, and is dedicated to delivering high quality video editing and animation solutions to the world.
"
HOMEPAGE="http://www.openshot.org/"
SRC_URI="https://github.com/OpenShot/openshot-qt/releases/download/v${PV}/OpenShot-v${PV}-x86_64.AppImage -> ${P}"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="strip"

DEPEND="!media-video/openshot"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"
APP="${DISTDIR}/${P}"
EXE="${S}/${P}"

src_prepare() {
	default
	cp ${APP} ${EXE}
}

src_install() {
	insinto /usr/share
	doins -r ${FILESDIR}/share/

	exeinto /usr/bin
	exeopts -m0755
	doexe ${EXE}

	domenu ${FILESDIR}/openshot.desktop

	dosym /usr/bin/${P} /usr/bin/${PN}
	dosym /usr/bin/${P} /usr/bin/openshot
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}

# Gentoo Overlay

自用软件备份。

> 配置大多参考其他 overlay 来源 gentoo-zh aur.archlinux.org
> 可以直接 fork 修改成自己需要的仓库

> `wps-office`字体问题可以通过字体本身来解决，目前测试迁移过来的字体，`simsum`显示正常，而`黑体`不行，使用`fc-list :lang=zh`可以查看显示字体，存在中文名称则可以正常显示。

## 软件仓库

| 名称         | 类别      | 备注                                                                                         |
| ------------ | --------- | -------------------------------------------------------------------------------------------- |
| freecad-bin  | media-gfx | `Appimage`版本，优秀的`3D`设计软件。                                                         |
| mockingbot   | media-gfx | 墨刀，原形软件，属于网页套壳，但平台上最好。开个会员够用。                                   |
| baidunetdisk | net-misc  | 百度网盘，采用`Archlinux AUR`的相关补丁，避免污染`$HOME`。稳定性差，但使用无问题。网页套壳。 |
| ocs-url      | x11-misc  | 方便安装桌面主题                                                                             |

## `GCC` 优化

尝试为 `GCC` 添加 `LTO + PGO` 优化。

> `-march=native` 编译出的包无法在其他型号 `CPU` 下使用。
>
> 因此在本机上 `CPU` 属于 `skylake` 架构，使用 `-march=native` 与 `-march=skylake` 编译出来的结果区别不大，
> 但不同的 `march` 会可能会指向不同的优化指令，可以使用 `gcc -c -Q -march=native --help=target` 进行查看。
>
> 可以在其他架构搭建包构建器，使用 `-march=skylake` 为本机编译二进制包。此内容参考 [二进制包构建指南][1]。

本次优化将参考 [`GCC 优化指南`][2]:

1. [`LinkTimeOptimization`][3];
2. [`Profile-guided optimization (PGO)`][4];
3. [`GCC 11 的高级优化和新功能`][6];

目前采用以下优化策略:

- `-march=native`，自动适配本机。特殊构建相应的内容查看 [Funtoo_CPU_Database][5];
- `-O3` 同时使用 `LTO + PGO` 使用该标志性能较优，见 [`OpenSUSE` 的测试][6];
- `-fomit-frame-pointer`，该标志明确指出，即使编译器默认会添加；
- `LDFLAGS` 尽量不做修改;
- `lto` 使用 `-flto=auto -fuse-linker-plugin -fno-fat-lto-objects`;
  `auto` 根据 `make -jn` 实现;
- `pgo` 优化相对比较麻烦，需要编译两次以实现特定的优化。

> `-flto-compression-level=n` 暂不确定可以如何指定 `zstd`，因此不做指定。

### 对 `gcc` 开启 `lto` 以及 `pgo`

```shell
sudo euse -E lto pgo -p sys-devel/gcc
# 重新编译 gcc
sudo emerge -aqv gcc
```

避免开启全局 `lto` 和 `pgo`，包含 `USE` 的程序都有自己的链接逻辑，无法自定义逻辑。

### 编译前确保开启快照 (`btrfs 系统下`)

```shell
# vim /etc/portage/bashrc
## SNAPPER
case "${EBUILD_PHASE}" in
    preinst)
        DESC="${CATEGORY}/${PF}"
        NUMBER=`snapper create -t pre -p -d "${DESC}"`
    ;;
    postinst)
        snapper create -t post --pre-number $NUMBER -d "${DESC}"
    ;;
esac
```

### 配置 `pgo` 到环境变量

`pgo` 由于会存在两次编译的情况，首先需要收集数据，再执行一次编译，以实现代码编译的优化。

但在收集数据的阶段会存在系统性能下降的情况，理论上是针对同一套代码实现的数据收集，版本升级后，原有收集的数据就没有作用了。

因此添加 `pgo` 优化后，需要低效率运行一段时间再重新构建一次系统。

> 损耗一部分时间以获得更好的优化?

这会导致升级系统步骤相对复杂。

```mermaid
flowchart LR
    pgo_update(使用PGO generate 更新软件)
    gather_data(收集一段时间PGO数据)
    world_update(整个系统更新)

    pgo_update --> gather_data
    gather_data --> |重新构建| world_update
```

即:

1. 添加一个专门用以收集 `PGO` 数据的环境，在更新软件是添加到 `package.env` 中，使得 `*/* pgo-generate`;
2. 使用一段时间后，删除该标志，使用系统的 `pgo-use` 环境重新构建，即 `emerge -e @world`;

系统开启 `PGO` 后第二次构建会很快，且同时开启了 `ccache`。

> 此处的问题在于，每次版本更新的时候第二次编译无法确定是哪些包执行编译，没有具体记录。此处需要考虑下如果优化这个步骤。
> 目前就是执行执行 `emerge -e @world`。
>
> 首次使用 `PGO` 优化，使用 `emerge --update --newuse --deep --with-bdeps=y @world`，
> 再重新构建 `emerge -eqv @world`。确保所有包可以涉及到。
>
> 除了新安装的包需要再次添加 `pgo-generate` 环境外，其他情况下，一律不再使用对应的标志。（更新以外）。
>
> 因此避免频繁更新代码。（一般不具有频繁更新的需求，除非打补丁）

添加 `pgo-generate` 环境如下:

```env
## PGO Generate
COMMON_FLAGS="-march=native -O3 -pipe"
COMMON_FLAGS="${COMMON_FLAGS} -fomit-frame-pointer"
## LTO
COMMON_FLAGS="${COMMON_FLAGS} -flto=auto -fuse-linker-plugin -fno-fat-lto-objects"
## PGO: need create /var/cache/pgo/portage
COMMON_FLAGS="${COMMON_FLAGS} -fprofile-generate"
COMMON_FLAGS="${COMMON_FLAGS} -fprofile-dir=/var/cache/pgo/portage"
CFLAGS="${COMMON_FLAGS}"
CXXFLAGS="${COMMON_FLAGS}"
FCFLAGS="${COMMON_FLAGS}"
FFLAGS="${COMMON_FLAGS}"
```

此处选择: `/var/cache/pgo/portage` 作为存储 `pgo` 数据路径，且创建 `btrfs 子卷` 存放;

> 注意这个文件夹必须全用户可读可写。

```shell
mount -t btrfs -o subvol=/ /dev/disk/by-label/MIBOOK /mnt/
cd /mnt
btrfs subvolume create @build@pgo
```

添加 `fstab`:

```
LABEL=MIBOOK /var/cache/pgo btrfs subvol=@build@pgo,compress=zstd,ssd,space_cache,discard=async,commit=120,nosuid,noatime,nodev,mode=1777,x-mount.mkdir=1777 0 0
```

> 该目录必须全用户可读可写！！!

配置 `/etc/sandbox.d`:

```conf
# /etc/sandbox.d/99pgo
SANDBOX_WRITE="/var/cache/pgo/portage"
# 默认全系统可读，因此不用添加可读
```

以下为目前的配置文件:

```env
# These settings were set by the catalyst build script that automatically
# built this stage.
# Please consult /usr/share/portage/config/make.conf.example for a more
# detailed example.
CHOST="x86_64-pc-linux-gnu"
COMMON_FLAGS="-march=native -O3 -pipe"
COMMON_FLAGS="${COMMON_FLAGS} -fomit-frame-pointer"
## LTO
COMMON_FLAGS="${COMMON_FLAGS} -flto=auto -fuse-linker-plugin -fno-fat-lto-objects"
## PGO: need create /var/cache/pgo/portage, and add sandbox.d conf
COMMON_FLAGS="${COMMON_FLAGS} -fprofile-use -fprofile-correction -Wno-error=missing-profile"
COMMON_FLAGS="${COMMON_FLAGS} -fprofile-dir=/var/cache/pgo/portage"
CFLAGS="${COMMON_FLAGS}"
CXXFLAGS="${COMMON_FLAGS}"
FCFLAGS="${COMMON_FLAGS}"
FFLAGS="${COMMON_FLAGS}"
CPU_FLAGS="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"

MAKEOPTS="-j4"
```

> 该构建似乎仅适用于无 `pgo lto` 标志的应用程序，包含两个标志的程序，会有自己的处理办法。
>
> 同时部分程序同时开启 `pgo` 和 `lto` 会编译不通过，会无法链接部分函数，此处优先处理方法为指定单独的 `pgo` 环境。

[1]: https://wiki.gentoo.org/wiki/Binary_package_guide
[2]: https://wiki.gentoo.org/wiki/GCC_optimization
[3]: https://gcc.gnu.org/wiki/LinkTimeOptimization
[4]: https://developer.ibm.com/articles/gcc-profile-guided-optimization-to-accelerate-aix-applications/
[5]: https://www.funtoo.org/Funtoo_CPU_Database
[6]: https://documentation.suse.com/sbp/server-linux/single-html/SBP-GCC-11/index.html

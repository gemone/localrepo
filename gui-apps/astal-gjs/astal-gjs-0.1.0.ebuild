# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit meson git-r3

DESCRIPTION="Building blocks for creating custom desktop shells"
HOMEPAGE="https://aylur.github.io/astal/"

EGIT_REPO_URI="https://github.com/aylur/astal"
EGIT_COMMIT="3468763"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="strip"

# TODO: add flags gtk3
DEPEND="
>=gui-apps/astal-${PV}:0
"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}"
# TODO: add use flags gtk3 gtk4
EMESON_SOURCE="${S}/lang/gjs"

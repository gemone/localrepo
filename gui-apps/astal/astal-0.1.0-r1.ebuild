# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit meson git-r3 vala

DESCRIPTION="Building blocks for creating custom desktop shells"
HOMEPAGE="https://aylur.github.io/astal/"

EGIT_REPO_URI="https://github.com/aylur/astal"
EGIT_COMMIT="3468763"

# FIXME: bad desgin, add use flags
# NEED TO USE valadoc
VALA_USE_DEPEND="valadoc"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="strip"

# TODO: add flags gtk3
DEPEND="
x11-libs/gtk+:3
gui-libs/gtk-layer-shell:0
dev-libs/gobject-introspection
gui-libs/gtk-layer-shell:0[vala,introspection]
$(vala_depend)
"

RDEPEND="
${DEPEND}
>=gui-apps/astal-io-${PV}
"

BDEPEND="${DEPEND}"

S="${WORKDIR}/${P}"
# TODO: add use flags gtk3 gtk4
EMESON_SOURCE="${S}/lib/astal/gtk3"

src_prepare() {
	vala_setup
	default
}

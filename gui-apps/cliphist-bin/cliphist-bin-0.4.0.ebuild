# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

APPNAME=cliphist-bin-${PV}

DESCRIPTION="clipboard history “manager” for wayland"
HOMEPAGE="https://github.com/sentriz/cliphist"
SRC_URI="https://github.com/sentriz/cliphist/releases/download/v${PV}/v${PV}-linux-amd64 -> ${APPNAME}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
	cp "`readlink -f ${DISTDIR}/${APPNAME}`" ${WORKDIR}
}

S="${WORKDIR}"

src_install() {
	exeinto /usr/bin
	doexe "${S}/${APPNAME}"

	dosym /usr/bin/${APPNAME} /usr/bin/cliphist
}

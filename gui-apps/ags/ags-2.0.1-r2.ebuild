# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 go-module

DESCRIPTION="A customizable and extensible shell"
HOMEPAGE="https://aylur.github.io/ags/"

SRC_URI="https://github.com/Aylur/ags/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
>=gui-apps/astal-gjs-0.1.0
dev-libs/gjs:0
dev-util/dart-sass:0
gui-libs/gtk-layer-shell
"

BDEPEND="virtual/pkgconfig"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}"

src_compile() {
	mkdir -p bin || die

	local go_ldflags="
    -X 'main.gtk4LayerShell=$(pkg-config --variable=libdir gtk4-layer-shell-0)/libgtk4-layer-shell.so' \
    -X 'main.astalGjs=$(pkg-config --variable=srcdir astal-gjs)'
	"

	local -a go_buildargs=(
		-ldflags "${go_ldflags}"
		-o bin
	)
	ego build "${go_buildargs[@]}"
}

src_install() {
	exeinto /usr/bin
	doexe "bin/${PN}"

	einstalldocs
}

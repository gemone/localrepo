# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit unpacker xdg

DESCRIPTION="Get MockingBot on desktop and mobile. Prototyping anytime, anywhere"
HOMEPAGE="https://modao.cc/"
SRC_URI="https://cdn.modao.cc/linux/${PN}_${PV}_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"

DEPEND="
	gnome-base/gconf
"
RESTRICT="strip"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"

src_install() {
	insinto  /usr/bin
	doins ${S}/usr/bin/mockingbot

	insinto /usr/lib
	doins -r ${S}/usr/lib/mockingbot

	insinto /usr/share
	doins -r ${S}/usr/share/{applications,pixmaps}

	if use doc; then
		dodoc ${S}/usr/share/doc/mockingbot/copyright
	fi

	fperms 0755 /usr/lib/mockingbot/MockingBot
}

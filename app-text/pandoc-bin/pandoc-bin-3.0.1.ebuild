# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Universal markup converter."
HOMEPAGE="https://pandoc.org/"
SRC_URI="https://github.com/jgm/pandoc/releases/download/${PV}/pandoc-${PV}-linux-${ARCH}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~arm64"
IUSE="+doc"
RESTRICT="strip"

DEPEND=""
# 避免冲突
RDEPEND="
${DEPEND}
!${CATEGORY}/pandoc
"
BDEPEND=""

S="${WORKDIR}/pandoc-${PV}"

src_unpack() {
	default

	gzip -d ${S}/share/man/man1/pandoc*.1.gz \
		|| die 'faild to decompress man page'
}

src_install() {
	insinto /usr/bin
	doins ${S}/bin/pandoc*

	if use doc; then
		doman ${S}/share/man/man1/pandoc*.1
	fi

	fperms +x /usr/bin/pandoc
}
